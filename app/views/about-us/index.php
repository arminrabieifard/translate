<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'درباره ما';
?>


<div class="about-us">
    <div class="title">
        <?= Html::encode($model->title)?>

    </div>
    <div class="description">
        <?= HtmlPurifier::process($model->description)?>
    </div>
</div>
