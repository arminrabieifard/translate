<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'لیست خبرها';
?>

<div class="row">
    <div class="">
    <?php foreach ($model as $new):?>
        <div class="col-sm-3">
            <a href="<?= Url::to(['view', 'id' => $new->id])?>">
                <div class="img">
                    <img class="img-responsive" src="<?= Yii::getAlias('@uploads-url') . '/news/' . $new->file_name ?>" alt="<?= Html::encode($new->title) ?>">
                </div>
                <div class="title">
                    <?= Html::encode($new->title) ?>
                </div>
            </a>
        </div>
    <?php endforeach;?>
    </div>
    <div class="">
        <?php
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
        ?>
    </div>
</div>