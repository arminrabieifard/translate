<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gilaki`.
 */
class m180102_193255_create_gilaki_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gilaki', [
            'id' => $this->primaryKey(),
            'title'=> $this->string(255)->notNull()->comment('عنوان'),
            'soundfile'=> $this->string(255)->notNull()->comment('فایل صوتی'),
            'has-pic'=> $this->boolean()->comment('عکس دارد؟'),
            'description'=> $this->text()->notNull()->comment('توضیحات کلمه'),
            'sample'=> $this->text()->notNull()->comment('نمونه کاربرد کلمه'),
            'create_at'=> $this->integer(11)->null()->defaultValue(0)->comment('زمان افزودن'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('gilaki');
    }
}
