<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085753_gallery extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%gallery}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'category_id'=> $this->integer(11)->unsigned()->notNull()->comment('مربوط به دسته'),
                'title'=> $this->string(255)->notNull()->comment('عنوان'),
                'description'=> $this->text()->null()->defaultValue(null)->comment('توضیحات'),
                'file_name'=> $this->string(50)->null()->defaultValue(null)->comment('نام فایل'),
                'images_file'=> $this->text()->null()->defaultValue(null)->comment('عکسهای ذخیره شده'),
                'num_view'=> $this->integer(11)->null()->defaultValue(0)->comment('تعداد بازدید'),
                'visible'=> $this->smallInteger(1)->null()->defaultValue(1)->comment('نمایش داده شود؟'),
                'create_at'=> $this->integer(11)->notNull()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );
        $this->createIndex('idx-gallery-category_id','{{%gallery}}',['category_id'],false);

    }

    public function safeDown()
    {
        $this->dropIndex('idx-gallery-category_id', '{{%gallery}}');
        $this->dropTable('{{%gallery}}');
    }
}
