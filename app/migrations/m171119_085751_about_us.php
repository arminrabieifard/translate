<?php

use yii\db\Schema;
use yii\db\Migration;

class m171119_085751_about_us extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%about_us}}',
            [
                'id'=> $this->primaryKey(11)->unsigned(),
                'title'=> $this->string(255)->notNull()->comment('عنوان'),
                'description'=> $this->text()->notNull()->comment('متن'),
                'create_at'=> $this->integer(11)->null()->defaultValue(0)->comment('زمان افزودن'),
            ],$tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%about_us}}');
    }
}
