<?php

use yii\db\Migration;

/**
 * Handles the creation of table `farsi`.
 */
class m180102_193739_create_farsi_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('farsi', [
            'id' => $this->primaryKey(),

            'title'=> $this->string(255)->notNull()->comment('عنوان'),

            'description'=> $this->text()->notNull()->comment('توضیحات کلمه'),

            'create_at'=> $this->integer(11)->null()->defaultValue(0)->comment('زمان افزودن'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('farsi');
    }
}
