<?php

use yii\db\Migration;

/**
 * Handles the creation of table `relfatogi`.
 */
class m180102_193927_create_relfatogi_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('relfatogi', [
            'id' => $this->primaryKey(),
            'gi_id' =>  $this->integer()->notNull()->comment('کد کلمه گیلکی'),
            'fa_id' =>  $this->integer()->notNull()->comment('کد کلمه فارسی'),
            'create_at'=> $this->integer(11)->null()->defaultValue(0)->comment('زمان افزودن'),
        ]);
        $this->addForeignKey(
            'fk-relgi_id',
            'relfatogi',
            'gi_id',
            'gilaki',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-relfa_id',
            'relfatogi',
            'fa_id',
            'farsi',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('relfatogi');
    }
}
