<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'گالری';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">

        <div class="gallery-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('افزودن گالری', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                            'id',
                            // 'category_id',
                            [
                                'attribute' => 'category_id',
                                'format' => 'raw',
                                'content' => function($data){
                                    return $data->category->title;
                                },
                            ],
                            'title',
                            'description:ntext',
                            'file_name',
                            // 'images_file:ntext',
                            // 'num_view',
                            // 'create_at',
                            [
                                'attribute' => 'create_at',
                                'format' => 'raw',
                                'content' => function($data){
                                    return \app\components\General::persianDate($data->create_at);
                                }
                            ],

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                <?php Pjax::end(); ?>
        </div>
    </div>
</div>
