<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\HtmlPurifier;
/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'درباره ما', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div class="box-body">
        <div class="about-us-view">

            <p>
                <?= Html::a('ویرایش', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <!--
                <?= Html::a('حذف', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'از حذف این گزینه اطمینان دارید؟',
                        'method' => 'post',
                    ],
                ]) ?>
                -->
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    // 'description:ntext',
                    [
                        'attribute' => 'description',
                        'format' => 'raw',
                        'value' => HtmlPurifier::process($model->description),
                    ],
                    //'create_at',
                    [
                        'attribute' => 'create_at',
                        'format' => 'raw',
                        'value' => \app\components\General::persianDate($model->create_at),
                    ]
                ],
            ]) ?>

        </div>
    </div>
</div>
